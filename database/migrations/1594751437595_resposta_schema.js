'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RespostaSchema extends Schema {
  up () {
    this.create('respostas', (table) => {
      table.increments()
      table.text('resposta').notNullable()
      table
        .integer('topico_id')
        .references('id')
        .inTable('topicos')
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.timestamps()
    })
  }

  down () {
    this.drop('respostas')
  }
}

module.exports = RespostaSchema
