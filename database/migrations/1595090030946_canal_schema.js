'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CanalSchema extends Schema {
  up () {
    this.create('canals', (table) => {
      table.increments()
      table
        .integer('curso_id')
        .unsigned()
        .references('id')
        .inTable('cursos')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.string('titulo').notNullable()
      table.string('descricao').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('canals')
  }
}

module.exports = CanalSchema
