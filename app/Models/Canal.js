'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Canal extends Model {
  user () {
    return this.belongsTo('App/Models/User')
  }

  curso () {
    return this.belongsTo('App/Models/Curso')
  }
}

module.exports = Canal
