'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Topico extends Model {
  categoria () {
    return this.belongsTo('App/Models/Categoria')
  }

  respostas () {
    return this.hasMany('App/Models/Resposta')
  }

  user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Topico
