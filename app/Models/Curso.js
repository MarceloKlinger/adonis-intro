'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Curso extends Model {
  user () {
    return this.belongsTo('App/Models/User')
  }

  canal () {
    return this.hasMany('App/Models/Canal')
  }
}

module.exports = Curso
