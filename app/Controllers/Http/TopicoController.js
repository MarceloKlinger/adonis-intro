'use strict'

const Topico = use('App/Models/Topico')

class TopicoController {
  async index ({ request, response }) {
    const topicos = await Topico.query()
      .with('categoria')
      .with('respostas')
      .with('user')
      .fetch()
    return response.status(200).send(topicos)
  }

  async store ({ request, response, auth }) {
    const { nome, descricao, categoria_id } = await request.post()

    const novoTopico = await Topico.create({
      nome,
      descricao,
      categoria_id,
      user_id: auth.user.id
    })
    return response.status(201).send(novoTopico)
  }
}

module.exports = TopicoController
