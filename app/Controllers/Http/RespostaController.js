'use strict'

const Resposta = use('App/Models/Resposta')

class RespostaController {
  async index ({ request, response }) {
    const respostas = await Resposta.all()
    return response.status(200).send(respostas)
  }

  async store ({ request, response, auth }) {
    const { resposta, topico_id } = await request.post()
    const novaResposta = await Resposta.create({
      resposta,
      topico_id,
      user_id: auth.user.id
    })
    return response.status(201).send(novaResposta)
  }
}

module.exports = RespostaController
