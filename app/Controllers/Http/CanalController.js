'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Canal = use('App/Models/Canal')

/**
 * Resourceful controller for interacting with canals
 */
class CanalController {
  /**
   * Show a list of all canals.
   * GET canals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, params }) {
    const canals = await Canal.query()
      .where('curso_id', params.cursos_id)
      .with('user')
      .fetch()

    return canals
  }

  /**
   * Render a form to be used for creating a new canal.
   * GET canals/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async store ({ request, params }) {
    const data = request.only([
      'user_id',
      'titulo',
      'descricao'
    ])

    const canal = await Canal.create({
      ...data,
      curso_id: params.cursos_id
    })

    return canal
  }

  /**
   * Display a single canal.
   * GET canals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params }) {
    const canal = await Canal.findOrFail(params.id)

    return canal
  }

  /**
   * Render a form to update an existing canal.
   * GET canals/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async update ({ params, request, response }) {
    const canal = await Canal.findOrFail(params.id)
    const data = request.only([
      'titulo',
      'user_id',
      'descricao'
    ])

    canal.merge(data)

    await canal.save()

    return canal
  }

  /**
   * Delete a canal with id.
   * DELETE canals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params }) {
    const canal = await Canal.findOrFail(params.id)

    await canal.delete()
  }
}

module.exports = CanalController
