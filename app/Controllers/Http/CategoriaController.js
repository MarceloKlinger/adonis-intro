'use strict'

const Categoria = use('App/Models/Categoria')

class CategoriaController {
  async index ({ response }) {
    const categorias = await Categoria.query()
      .with('user')
      .fetch()

    return response.status(200).send(categorias)
  }

  async store ({ request, auth }) {
    const data = await request.only(['nome'])

    const categoria = await Categoria.create({
      ...data,
      user_id: auth.user.id
    })
    return categoria
  }
}

module.exports = CategoriaController
