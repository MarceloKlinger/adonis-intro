<h1 align="center" >
    <br>
    Challenge Meetup PRODAP
</h1>
<p align="center">
  <a href="#rocket-tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#information-source-como-usar">Como usar</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#memo-license">Licença</a>
</p>
<h1><img src="https://ik.imagekit.io/k9nuvlg2vw/Captura_de_Tela_2020-07-18_a_s_19.18.06_07Ur12yVJ.png" alt="page" /></>
<p align="center">
</p>

## :rocket: Tecnologias

-  [AdonisJs](https://adonisjs.com/)
-  [JavaScript](https://www.javascript.com/)
-  [Insomnia Rest](https://insomnia.rest/)
-  [DBeaver](https://dbeaver.io/)
-  [VS Code][vc]
-  [Docker](https://www.docker.com/)
  
## :information_source: Como usar


```bash
# Clone o repositório
$ git clone git@gitlab.com:MarceloKlinger/adonis-intro.git

# Entre na pasta adonis-intro
$ cd adonis-intro

# Config arquivo .env

HOST=127.0.0.1
PORT=3333
NODE_ENV=development

APP_NAME=AdonisJs
APP_URL=http://${HOST}:${PORT}

CACHE_VIEWS=false

APP_KEY=

DB_CONNECTION=sqlite
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER=root
DB_PASSWORD=
DB_DATABASE=adonis

HASH_DRIVER=bcrypt


# Crie as migrations
$ adonis migration:run

# Inicie o servidor 
$ adonis server --dev

# Importando as configurações do Insomnia com todas as rotas configuradas conforme mostra a imagem abaixo

#meetup_adonis.json

```
<h1><img src="https://ik.imagekit.io/k9nuvlg2vw/Captura_de_Tela_2020-07-19_a_s_19.17.24_xXKuMk-3Z.png" alt="page" /></>
<p align="center">
</p>

## :memo: License
This project is under the MIT license. See the [LICENSE](https://gitlab.com/MarceloKlinger/adonis-intro/master/LICENCE) for more information.

---

Marcelo Klinger :wave: [Get in touch!](https://www.linkedin.com/in/marcelo-klinger-santos-4744a6140/)

[vc]: https://code.visualstudio.com/
