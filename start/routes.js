'use strict'

const Route = use('Route')

Route.get('/test', () => {
  return { greeting: 'Hello world in AdonisJS' }
})

Route.post('/users', 'UserController.store')
Route.post('/sessions', 'SessionController.store')

Route.group(() => {
  Route.resource('/categorias', 'CategoriaController').only(['index', 'store'])
  Route.resource('/topicos', 'TopicoController').only(['index', 'store'])
  Route.resource('/respostas', 'RespostaController').only(['index', 'store'])

  Route.resource('/cursos', 'CursoController').apiOnly()
  Route.resource('/cursos.canals', 'CanalController').apiOnly()
}).prefix('api').middleware(['auth:jwt'])
